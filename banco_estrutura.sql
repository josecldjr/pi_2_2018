-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 19-Nov-2018 às 22:27
-- Versão do servidor: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alocacao_hora`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cargos`
--

CREATE TABLE `cargos` (
  `ID_cargo` int(11) NOT NULL,
  `descricao` varchar(100) NOT NULL,
  `ID_usuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `condicao`
--

CREATE TABLE `condicao` (
  `ID_condicao` int(11) NOT NULL,
  `pendente` tinyint(1) DEFAULT NULL,
  `iniciada` tinyint(1) DEFAULT NULL,
  `em_andamento` tinyint(1) DEFAULT NULL,
  `finalizada` tinyint(1) DEFAULT NULL,
  `reprovada` tinyint(1) DEFAULT NULL,
  `aprovada` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pausas`
--

CREATE TABLE `pausas` (
  `ID_pausa` int(11) NOT NULL,
  `tipo_pausa` varchar(255) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `tempo` time NOT NULL,
  `ID_tarefa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `projetos`
--

CREATE TABLE `projetos` (
  `ID_projeto` int(11) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `ID_tarefa` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tarefas`
--

CREATE TABLE `tarefas` (
  `ID_tarefa` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `horas_atribuidas` time NOT NULL,
  `horas_gastas` time DEFAULT NULL,
  `hora_inicial` time DEFAULT NULL,
  `hora_final` time DEFAULT NULL,
  `ID_projeto` int(11) DEFAULT NULL,
  `ID_condicao` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `ID_usuario` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `senha` varchar(8) NOT NULL,
  `ID_cargo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cargos`
--
ALTER TABLE `cargos`
  ADD PRIMARY KEY (`ID_cargo`),
  ADD KEY `fk_Usuario_Cargo` (`ID_usuario`);

--
-- Indexes for table `condicao`
--
ALTER TABLE `condicao`
  ADD PRIMARY KEY (`ID_condicao`);

--
-- Indexes for table `projetos`
--
ALTER TABLE `projetos`
  ADD PRIMARY KEY (`ID_projeto`),
  ADD KEY `fk_Tarefa_Projeto` (`ID_tarefa`);

--
-- Indexes for table `tarefas`
--
ALTER TABLE `tarefas`
  ADD PRIMARY KEY (`ID_tarefa`),
  ADD KEY `fk_Projeto_Tarefa` (`ID_projeto`),
  ADD KEY `fk_Condicao_Tarefa` (`ID_condicao`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`ID_usuario`),
  ADD KEY `fk_Cargo_Usuario` (`ID_cargo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cargos`
--
ALTER TABLE `cargos`
  MODIFY `ID_cargo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `condicao`
--
ALTER TABLE `condicao`
  MODIFY `ID_condicao` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `projetos`
--
ALTER TABLE `projetos`
  MODIFY `ID_projeto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tarefas`
--
ALTER TABLE `tarefas`
  MODIFY `ID_tarefa` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `ID_usuario` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `cargos`
--
ALTER TABLE `cargos`
  ADD CONSTRAINT `fk_Usuario_Cargo` FOREIGN KEY (`ID_usuario`) REFERENCES `usuarios` (`ID_usuario`);

--
-- Limitadores para a tabela `projetos`
--
ALTER TABLE `projetos`
  ADD CONSTRAINT `fk_Tarefa_Projeto` FOREIGN KEY (`ID_tarefa`) REFERENCES `tarefas` (`ID_tarefa`);

--
-- Limitadores para a tabela `tarefas`
--
ALTER TABLE `tarefas`
  ADD CONSTRAINT `fk_Condicao_Tarefa` FOREIGN KEY (`ID_condicao`) REFERENCES `condicao` (`ID_condicao`),
  ADD CONSTRAINT `fk_Projeto_Tarefa` FOREIGN KEY (`ID_projeto`) REFERENCES `projetos` (`ID_projeto`);

--
-- Limitadores para a tabela `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_Cargo_Usuario` FOREIGN KEY (`ID_cargo`) REFERENCES `cargos` (`ID_cargo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
