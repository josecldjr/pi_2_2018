<div class="container-fluid">
        <div class="cabecalho">
            <div class="row">
                <div class="col col-md-2">
                    <img src="<?= base_url('img/tarefa.png') ?>" class="rounded float-left" width="85px" alt="TAREFAS">
                </div>
                <div class="col col-md-3">
                    <h1 style="font-size: 40px;margin-left: 40px;">NOVA TAREFA</h1>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <!-- <div class="container-fluid">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-5">
                <div class="header-content">
                    <div class="header-content-inner redon" style="background-color:black">
                        <input type="text" name="" value="NOME TAREFA" style="font-family: HammersmithOne;font-size: 14pt; text-align: center"
                            class="form-control" id="nomeTarefa" disabled>
                        <div id="contenedor">
                            <div class="reloj" id="Horas">00</div>
                            <div class="reloj" id="Minutos">:00</div>
                            <div class="reloj" id="Segundos">:00</div>
                            <div class="reloj" id="Centesimas">:00</div>
                        </div>
                        <div class="text-center">
                            <input type="button" class="btn btn-danger" style="padding: 10px;" id="inicio" value="Iniciar"
                                onclick="inicio(); ">
                            <input type="button" class="btn btn-danger" style="padding: 10px;" id="parar" value="Parar"
                                onclick="parar();" disabled>
                            <input type="button" class="btn btn-danger" style="padding: 10px;" id="continuar" value="Retornar &#8634;"
                                onclick="inicio();" disabled>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="container-fluid">
                    <table class="table table-bordered" id="table_tarefa" style="-moz-border-radius: 8px;
                    -webkit-border-radius: 8px;
                    border-radius: 8px;
                    border: 1;">

                        <thead style="background-color:#337ab7;">
                            <tr>
                                <th scope="col" style="font-family: HammersmithOne; color:
                                    white;font-size: 12pt; text-align: center;">tipo
                                    pausa</th>
                                <th scope="col" style="font-family: HammersmithOne; color:
                                     white;font-size: 12pt; text-align: center;">descricao
                                    pausa
                                    PAUSA</th>
                                <th scope="col" style="font-family: HammersmithOne; color:
                                     white;font-size: 12pt; text-align: center;">tempo</th>
                            </tr>
                        </thead>
                        <tbody style="font-family: HammersmithOne;color: #337ab7;text-align: center">

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-5"></div>
        </div>
    </div> -->

    <hr>
    <div class="container-fluid">
        <div class="row" style="background-color:#337ab7;
                -moz-border-radius: 8px;
                -webkit-border-radius: 8px;
                border-radius: 8px;">
            <h4 style="font-family: HammersmithOne; color: white;font-size: 14pt; text-align: center;">
                DETALHES DA TAREFA
            </h4>
        </div>
        <br>
        <div class="row">
            <form id="form">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputNome">NOME:</label>
                        <input type="text" name="nome" class="form-control" id="inputNome">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputDescricao">DESCRIÇÃO:</label>
                        <textarea class="form-control"  name="descricao" id="textArea" rows="3"></textarea>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="selectCategoria">Projeto:</label>
                        <select name="ID_projeto" class="form-control select_per" id="selectCategoria">
						   <option value="0"></option>
						   <?php foreach ($projetos as $key => $projeto) { ?>
							<option value="<?= $projeto['ID_projeto'] ?>"><?= $projeto['nome'] ?></option>
						   <?php } ?>
						   
                        </select>
                    </div>
                    <!-- <div class="form-group col-md-3">
                        <label for="selectCategoria">TIPO:</label>
                        <select class="form-control select_per" id="selectTipo" disabled>
                            <option>CRIAR SCRIPT</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div> -->
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="tempoAtribuido">HORAS ATRIBUÍDAS:</label>
                        <input type="time" class="form-control" name="horas_atribuidas" id="tempoAtribuido">
                    </div>
                    <!-- <div class="form-group col-md-3">
                        <label for="tempoGasto">HORAS GASTAS:</label>
                        <input type="time" class="form-control" id="tempoGasto">
                    </div> -->
                </div>

                <div class="form-row">
                    <!-- <div class="form-group col-md-6">
                        <label for="inputDescricao">OBSERVAÇÃO:</label>
                        <textarea class="form-control" id="textArea" rows="3"></textarea>
                    </div> -->
                    <div class="form-group col-md-3">

                    </div>
                    <div class="form-group col-md-3">
                        <button type="submit" class="btn btn-danger" style="padding:10px; margin-top: 15%;">
							<b>SOLICITAR TAREFA</b>
						</button>
                    </div>
                </div>
            </form>
		</div>
<!-- 		
		<div class="modal fade" id="novaPausaTarefa" tabindex="-1" role="dialog" aria-labelledby="novoPausa" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title" style="font-family: HammersmithOne;color: #337ab7;">Nova Pausa</h2>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-row">
                            <div class="form-group">
                                <label for="ipDescricao">Descrição</label>
                                <input type="textarea" class="form-control" id="ipDescricao">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group">
                                <label for="slTipoPausa">Tipo Pausa</label>
                                <select id="slTipoPausa" class="form-control">
                                    <option selected>Selecione</option>
                                    <option>Café</option>
                                    <option>Almoço</option>
                                    <option>Reunião</option>
                                    <option>Outra tarefa </option>
                                    <option> ida ao banheiro </option>

                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary bnt_per" data-dismiss="modal">Fechar</button>
                    <button type="button" class="btn btn-primary bnt_per" id="bntSalvar" onclick="salvarPausa();">Salvar</button>
                </div>
            </div>
        </div> -->
	</div>
	<script>
		$('#form').on('submit', e=>{
			e.preventDefault();

			let data = {};
			$('#form')
			 .serializeArray()
			 .forEach(function(e, i, a){
				 data[e['name']] = e['value'];
			 });

			  criarSolicitacao(data);
			
		});

		function criarSolicitacao(solicitacao){

			$.ajax({
				url:'<?= base_url('api/tarefas/create')?>',
				method:'post',
				data:solicitacao
			})
			.success(function(r){
				r = JSON.parse(r);

				window.location.replace('<?= base_url() ?>');
			})
			.fail(function(){
				alert('Falha ao cria a solicitacao de tarefa');
			});
		}
		
	
	</script>
