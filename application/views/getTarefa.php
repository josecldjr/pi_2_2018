<div class="container-fluid">
        <div class="cabecalho">
            <div class="row">
                <div class="col col-md-2">
                    <img src="<?= base_url('img/tarefa.png')?>" class="rounded float-left" width="85px" alt="TAREFAS">
                </div>
                <div class="col col-md-3">
                    <h1 style="font-size: 40px;margin-left: 40px;">TAREFA</h1>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-5">
                <div class="header-content">
                    <div class="header-content-inner redon" style="background-color:black">
                        <input type="text" name="" value="<?= $tarefa['nome'] ?>" style="font-family: HammersmithOne;font-size: 14pt; text-align: center"
                            class="form-control" id="nomeTarefa" disabled>
                        <div id="contenedor" title="Horas gastas">
                            <div class="reloj" id="Horas"><?= explode(':',$tarefa['horas_gastas'])[0] ?></div>
                            <div class="reloj" id="Minutos">:<?= explode(':',$tarefa['horas_gastas'])[1] ?></div>
                            <div class="reloj" id="Segundos">:<?= explode(':',$tarefa['horas_gastas'])[2] ?></div>
                            <div class="reloj" id="Centesimas">:00</div>
                        </div>
                        <div class="text-center">
                            <input type="button" class="btn btn-danger" style="padding: 10px;" id="inicio" value="Iniciar"
                                onclick="inicio(); ">
                            <input type="button" class="btn btn-danger" style="padding: 10px;" id="parar" value="Parar"
                                onclick="parar();" disabled>
                            <input type="button" class="btn btn-danger" style="padding: 10px;" id="continuar" value="Retornar &#8634;"
                                onclick="inicio();" disabled>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="container-fluid">
                    <table id="jsGrid" class="table table-bordered" id="table_tarefa" style="-moz-border-radius: 8px;
                    -webkit-border-radius: 8px;
                    border-radius: 8px;
                    border: 1;">
                        <thead style="background-color:#337ab7;">
                            <tr>
                                <th scope="col" style="font-family: HammersmithOne; color:
                                    white;font-size: 12pt; text-align: center;">tipo
                                    pausa</th>
                                <th scope="col" style="font-family: HammersmithOne; color:
                                     white;font-size: 12pt; text-align: center;">descricao
                                    pausa
                                    PAUSA</th>
                                <th scope="col" style="font-family: HammersmithOne; color:
                                     white;font-size: 12pt; text-align: center;">tempo</th>
                            </tr>
                        </thead>
                        <tbody style="font-family: HammersmithOne;color: #337ab7;text-align: center">

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-5"></div>
        </div>
    </div>

    <hr>
    <div class="container-fluid">
        <div class="row" style="background-color:#337ab7;
                -moz-border-radius: 8px;
                -webkit-border-radius: 8px;
                border-radius: 8px;">
            <h4 style="font-family: HammersmithOne; color: white;font-size: 14pt; text-align: center;">
                DETALHES DA TAREFA
            </h4>
        </div>
        <br>
        <div class="row">
            <form>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputNome">NOME:</label>
                        <input type="text" class="form-control" id="inputNome" disabled value="<?= $tarefa['nome'] ?>">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputDescricao">DESCRIÇÃO:</label>
                        <textarea class="form-control" id="textArea" rows="3" disabled><?= $tarefa['descricao'] ?></textarea>
                    </div>
                </div>
                <div class="form-row">
                    <!-- <div class="form-group col-md-3">
                        <label for="selectCategoria">CATEGORIA:</label>
                        <select class="form-control select_per" id="selectCategoria" disabled>
                            <option>BANCO DE DADOS</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div> -->
                    <!-- <div class="form-group col-md-3">
                        <label for="selectCategoria">TIPO:</label>
                        <select class="form-control select_per" id="selectTipo" disabled>
                            <option>CRIAR SCRIPT</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div> -->
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="tempoAtribuido">HORAS ATRIBUÍDAS:</label>
                        <input type="time" class="form-control" id="tempoAtribuido" value="<?= $tarefa['horas_atribuidas'] ?>" disabled >
                    </div>
                    <div class="form-group col-md-3">
                        <label for="tempoGasto">HORAS GASTAS:</label>
                        <input type="time" class="form-control" id="tempoGasto" value="<?= $tarefa['horas_gastas'] ?>" disabled>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputDescricao">OBSERVAÇÃO:</label>
                        <textarea class="form-control" id="textArea" rows="3"></textarea>
                    </div>
                    <div class="form-group col-md-3">

                    </div>
                    <div class="form-group col-md-3">
                        <button type="button" onclick="atualizarHorasGastas(); window.location.replace('<?= base_url() ?>')" class="btn btn-danger" style="padding:10px; margin-top: 15%;">
							<b>FINALIZAR TAREFA</b>
						</button>
                    </div>
                </div>
            </form>
		</div>
		
		<div class="modal fade" id="novaPausaTarefa" tabindex="-1" role="dialog" aria-labelledby="novoPausa" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title" style="font-family: HammersmithOne;color: #337ab7;">Nova Pausa</h2>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-row">
                            <div class="form-group">
                                <label for="ipDescricao">Descrição</label>
                                <input type="textarea" class="form-control" id="ipDescricao">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group">
                                <label for="slTipoPausa">Tipo Pausa</label>
                                <select id="slTipoPausa" class="form-control">
                                    <option selected value="Outros">Selecione</option>
                                    <option>Café</option>
                                    <option>Almoço</option>
                                    <option>Reunião</option>
                                    <option>Outra tarefa </option>
                                    <option> ida ao banheiro </option>

                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary bnt_per" data-dismiss="modal">Fechar</button>
					<button type="button" class="btn btn-primary bnt_per" id="bntSalvar" 
					onclick="insertPausa(<?= $idTarefa ?>,salvarPausa());">Salvar</button>
                </div>
            </div>
        </div>
    </div>
	<script>
		updateTable();

		setInterval(function(){
			atualizarHorasGastas();
		}, 3000);

		function atualizarHorasGastas(){
			var Horas = document.getElementById("Horas").innerHTML
			var Minutos = document.getElementById("Minutos").innerHTML
			var Segundos = document.getElementById("Segundos").innerHTML
			var Centesimas = document.getElementById("Centesimas").innerHTML
			
			console.log({
					horas_atribuidas: (Horas + Minutos + Segundos),
					id:<?= $idTarefa ?>
					});
			
			$.ajax({
				url:'<?= base_url('api/tarefas/alter') ?>',
				method:'post',
				data:{
					horas_gastas: (Horas + Minutos + Segundos),
					id:<?= $idTarefa ?>
					}
			})
			.success(function(){
				console.log('<?= base_url('api/tarefas/alter') ?>');
				
			})
			.fail(function(){
				console.log('Houve algum erro ao setar a hora')
			})
		}

		/**
		 * busca informações bia ajax e atualiza a tabela
		 */
		function updateTable(){

			

			$.ajax({
				url:'<?= base_url('api/tarefas/listPausas?id=' . $idTarefa) ?>',
				method:'GET'
			})
			.success(function(r){
				r = JSON.parse(r);
				console.log(r);
				

				drawTable(r);
			})
			.fail(function (){
				// tratameto de erros
				setTimeout(function(){
					alert('Falha ao buscar informações sobre pasas');
				}, 3000);
			});
		}	
		
		/**
		 * Desenha a tabela
		 * @arg array com os valores que serão mostrados
		 */
		function drawTable(content){
			$('#jsGrid').jsGrid({
				width:"100%",
				height:"245px",
				sorting: true,
           		paging: true,
				pageSize:6,
				pageButtonCount:5,
				data:content,
				fields:[
					{ name:'tipo_pausa' ,title: 'Tipo pausa'},
					{ name:'descricao' ,title: 'Decrição Pausa'},
					{ name:'tempo' ,title: 'Tempo', width:100}
				]

			})
		}
			
		/**
		 * Insere uma pausa
		 * @arg id id da solicitaçao
		 * @arg pausa objto a pausa que será inserida
		 */
		function insertPausa(id,pausa){
			pausa['idtarefa'] = id;

			$.ajax({
				url:'<?= base_url('api/tarefas/insertPausa') ?>',
				method:'post',
				data:pausa
			})
			.success(function(r){
				r = JSON.parse(r);

				if (r == true){
					updateTable();
				}
				else{
					alert('Falha ao inserir pausa');
				}
			})
			.fail(function(){
				alert('Falha ao inserir pausa');
			})
		}

	
	</script>
