<?php
	if (!(isset($user))){
		redirect(base_url('main/login'));
	}
 
?>

<nav class="navbar navbar-dark bg-primary">

<div class="container-fluid">
	<a class="navbar-brand fontPerson" href="<?= base_url('') ?>">GESTÃO DE PRODUÇÃO</a>
	<a class="navbar-brand fontPerson" style="font-size:10pt" href="<?= base_url('main/usuarios') ?>">USUÀRIO</a>
	<a class="navbar-brand fontPerson" style="font-size:10pt" href="<?= base_url('main/projetos') ?>">PROJETO</a>
	<a class="navbar-brand fontPerson" style="font-size:10pt" href="<?= base_url('main/novaTarefa') ?>" <?= !$user->admin ? 'hidden' : '' ?>>ABRIR SOLICITAÇÃO</a>
	<a class="navbar-brand fontPerson" style="font-size:10pt" href="<?= '/dashboard' ?>" <?= !$user->admin ? 'hidden' : '' ?>>DASHBOARD</a>

	<div class="navbar-right ">
		<span class="navbar-text fontPerson">
			Signed in as <span id="usuarioNome"> Mark Otto</span>
		</span>
		<button class="btn btn-danger" style="margin-top: 3%;margin-left: -3%;" id="logout" onclick="logout()" type="button">Sair</button>
	</div>
</div>
</nav>
<script>
	function logout(){
		window.location.replace('<?= base_url('main/doLogout') ?>');
	}
</script>
