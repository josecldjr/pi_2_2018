<div id="id_master_page"> </div>
    <div class="container-fluid">
        <div class="cabecalho">
            <div class="row">
                <div class="col col-md-2">
                    <img src="<?= base_url('img/projeto.png') ?>" class="rounded float-left" width="100px" alt="PROJETOS">
                </div>
                <div class="col col-md-3" style="margin-left: 70px;">
                    <h1 style="font-size: 40px;">PROJETOS</h1>
                </div>
            </div>
        </div>
        <div>
            <hr>
            <div class="container-fluid">
                <div class="row">
                    <button class="btn btn-danger bnt_per" id="bnt_Incluir"
                        data-toggle="modal" data-target="#novoProjeto">INCLUIR</button>
                    <button class="btn btn-danger bnt_per" id="bnt_Editar" disabled>EDITAR</button>
                </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="novoProjeto" tabindex="-1" role="dialog" aria-labelledby="novoProjeto"
                aria-hidden="true">
                <form id="form" class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h2 class="modal-title" style="font-family: HammersmithOne;color: #337ab7;">Novo Projeto</h2>
                        </div>
                        <div class="modal-body">
                            <div id="form">
                                <div class="form-row">
                                    <div class="form-group">
                                        <label for="iptnome">Nome Projeto:</label>
                                        <input type="text" name="nome" class="form-control" id="iptnome">
                                    </div>
                                </div>
                                <div class="form-row">
                                        <!-- <div class="form-group">
                                            <label for="slGestor">Gestor do Projeto</label>
                                            <select id="slGestor" class="form-control">
                                                    <option selected>Selecione</option>
                                                    <option>Funcionario1</option>
                                                    <option>Funcionario2</option>
                                                    <option>Funcionario3</option>
                                                </select>
                                        </div> -->
                                    </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary bnt_per" data-dismiss="modal">Fechar</button>
                            <input type="submit" class="btn btn-primary bnt_per" value="Salvar" id="bntSalvar"> 
                        </div>
                    </div>
                </form>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col col-md-1">
                    </div>
                    <div id="jsGrid" class="col col-md-10">
                        <!--GRID-->
                    </div>
                    <div class="col col-md-1"></div>
                </div>
			</div>
			<script>

				$('#form').on('submit',function(e){
					e.preventDefault();

					var data = {};

					$('#form')
					.serializeArray()
					.forEach(function(e, i, a){
						data[e['name']] = e['value'];
					});

					createProjeto(data);
				})
				

				atualizarTabela();

				function createProjeto(projeto){


					console.log(projeto);
					
					$.ajax({
						url:'<?= base_url('api/projetos/create') ?>',
						method:'post',
						data:projeto
					})
					.success( r => {
						atualizarTabela();
					})
					.fail( e=>{
						alert('Falha ao criar projeto', e);
					});
				}
				
				function atualizarTabela(){

					$.ajax({
						url:'<?= base_url('api/projetos/list') ?>',
						method:'get'
					})
					.success(function(r){
						r = JSON.parse(r);
						console.log(r);

						desenharTabela(r);

					})
					.fail(function(){
						alert('Erro ao buscar informações sobre o projeto');
					});
				}

				function desenharTabela(content){
					$("#jsGrid").jsGrid({
						width: "100%",
						height: "330px", 
						sorting: true,
						paging: true,

						data: content,

						fields: [
							{ name: 'ID_projeto', title: "ID", width:10 },
							{ name: 'nome', title: "Nome", },
							

						],
						rowClick: function (args) {
							console.log(args)
							var getData = args.item;
							var keys = Object.keys(getData);
							var resultado = []
							$.each(keys, function (idx, value) {
								resultado.push(value + " : " + getData[value])
							});

							alert(resultado);
						},

						loadComplete: function fontFormatter(cellValue, opts, rowObject) {
							switch (rowObject.col1) {
								case "1":
									return '<span style="color:red">' + cellValue + '</span>';
									break;
								case "2":
									return '<span style="color:green">' + cellValue + '</span>';
									break;
							}
						}
					});
				}
			
			</script>
