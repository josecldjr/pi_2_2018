<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct(){
		parent::__construct();
		
		$this->load->helper('cookie');

		$this->load->model('cargo_model','cargomodel');
		$this->load->model('tarefas_model','tarefasmodel');
		$this->load->model('projetos_model','projetosmodel');
 		
	}
	 
	public function __destruct(){
	
	}
	public function index()
	{	
		
		$data['user'] =  json_decode(get_cookie('user'));

		$this->load->view('template/html_header',$data);
		$this->load->view('template/master_page'); 
		$this->load->view('principal.php'); 
		$this->load->view('template/html_footer');
	}


	public function login(){
		$this->load->view('login.php');

	}

	public function doLogin($id){
 
		// força um login de mock usando cookies
		switch ($id){
			case '31':// admin	
					
				setcookie(
				 'user',
				 json_encode([
					'id' => '55',
					'userName' => 'Fausto S.',
					'userEmail' => 'faustosuarez@devinfor.com.br',
					'admin' => true
				 ]),
				 0,
				 '/'); 


				//  var_dump(get_cookie('user'));
				break;
			case '55':// user
							
				setcookie(
					'user',
					json_encode([
					   'id' => '55',
					   'userName' => 'Judite M.',
					   'userEmail' => 'juditemafra@devinfor.com.br',
					   'admin' => false
					]),
					0,
					'/'); 
				break;
		}

		redirect( base_url() );
	}

	public function doLogout(){
		
		setcookie('user', '', time()-1);
		
		redirect( base_url('main/login') );
	}

	public function usuarios(){

		$data['cargos'] = $this->cargomodel->list();
		$data['user'] =  json_decode(get_cookie('user'));
	 
		$this->load->view('template/html_header',$data);
		$this->load->view('template/master_page'); 
		$this->load->view('usuarios'); 
		$this->load->view('template/html_footer');
	}

	public function projetos(){

		$data['user'] =  json_decode(get_cookie('user'));

		$this->load->view('template/html_header', $data);
		$this->load->view('template/master_page'); 
		$this->load->view('projetos'); 
		$this->load->view('template/html_footer');
	}

	public function getTarefa( $id ){

		$data['user'] =  json_decode(get_cookie('user'));

		// busca a tarefa no banco à partir do id na uri
		$data['tarefa'] = $this->tarefasmodel->get($id);	
		$data['idTarefa'] = $id; 
		
		$this->load->view('template/html_header', $data);
		$this->load->view('template/master_page'); 
		$this->load->view('getTarefa'); 
		$this->load->view('template/html_footer');
	}

	public function novaTarefa(){

		$data['user'] =  json_decode(get_cookie('user'));

		$data['projetos'] = $this->projetosmodel->list();

		$this->load->view('template/html_header',$data);
		$this->load->view('template/master_page'); 
		$this->load->view('novaTarefa'); 
		$this->load->view('template/html_footer');
	}
}
