<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class tarefas_model extends CI_Model {

	 
 
	public function create($camps){
		
		if (!(is_array($camps)) ){
			return false;
		}		 
		
		$result = $this->db->insert('condicao',['em_andamento' => 1]);
		$camps['ID_condicao'] = $this->db->insert_id();

		if ($result){
			return $this->db->insert('tarefas', $camps);
		}

		  

		return $result;
	}

	public function list(){
		
		$this->db->select('t.ID_tarefa , t.nome , t.descricao, 
						   t.horas_atribuidas, t.horas_gastas, t.hora_inicial,
						   t.hora_final, t.ID_projeto, t.ID_condicao, 
						   c.finalizada, c.reprovada, c.aprovada,
						   c.pendente, c.iniciada, c.em_andamento'	
						);   
		$this->db->from('tarefas t');
		$this->db->join('condicao c', 'c.ID_condicao = t.ID_condicao', 'left');

		$result = $this->db->get();

		
		return $result->result_array();
	}

	public function get($id){
		
		$this->db->select('t.ID_tarefa , t.nome , t.descricao, 
						   t.horas_atribuidas, t.horas_gastas, t.hora_inicial,
						   t.hora_final, t.ID_projeto, t.ID_condicao, 
						   c.finalizada, c.reprovada, c.aprovada,
						   c.pendente, c.iniciada, c.em_andamento'
							);   
		$this->db->from('tarefas t');
		$this->db->join('condicao c', 'c.ID_condicao = t.ID_condicao', 'left');
		$this->db->where('t.ID_tarefa =',$id);
		
		$result = $this->db->get()->result_array();
 
		if ( count($result) > 0 ){
			$result = $result[0];
		}
		else{
		 
			$result = false;
		} 

		
		return $result;
	}

	public function alter($id, $camps){
		
		if (!(is_array($camps))){
			return false;
		}
	
		$this->db->where('ID_tarefa',$id);
		return $this->db->update('tarefas',$camps); 	
	}

	public function delete($id){
	
		$result = $this->db->delete('tarefas','ID_tarefa = '.$id);
		
		return $result;
	}

	public function insertPausa($data){
		
		// verifica se os campos estão setados
		if (!(
			   isset($data['tipo_pausa'])
			|| isset($data['descricao_pausa'])
			|| isset($data['tempo'])
			|| isset($data['ID_tarefa'])
		)){
			return false;
		}

		return $this->db->insert('pausas',$data);
	}
	
	public function listPausas($id_tarefa){
		$this->db->select('ID_pausa, tipo_pausa, descricao, tempo, ID_tarefa');
		$this->db->from('pausas');
		$this->db->where('ID_tarefa =', $id_tarefa);

		return $this->db->get()->result_array();
	}
}
